# Use the official Golang image as the base image
FROM golang:1.17-alpine AS builder


# Set the working directory inside the container
WORKDIR /app

# Copy the Go module files and download dependencies
COPY go.mod .
# generated go.sum using go tidy command on terminal manually, it contained checksum for dependecies from go.mod to compare them
COPY go.sum .     
# go mod download will download all from go.mod    
RUN go mod download

# Copy the rest of the source code from the local to inside the container
COPY . .

# Build the Go binary by using go build which will run any file .go  (in our case is main.go)
RUN go build -o /app/beetroot ./cmd/beetroot

# Final stage: Use a smaller image for the runtime
FROM alpine:latest

# Set the working directory inside the container
WORKDIR /app

# Copy the binary from the builder stage
COPY --from=builder /app/beetroot .

# Expose the port that the application runs on
EXPOSE 8080

# Command to run the binary
CMD ["./beetroot"]
